package com.example.webservice2;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/groups")
@AllArgsConstructor
@CrossOrigin
public class GroupController {
    GroupService groupService;


    @PostMapping("/create/")
    public Group createGroup(@RequestParam String groupName) {
        return groupService.createGroup(groupName);
    }

    @PostMapping("/addPersonToGrup/{groupId}")
    public Group addPerson(@RequestParam String personId, @PathVariable String groupId){
        return groupService.addPersonToGroup(personId, groupId);
    }

    @GetMapping("/get/{id}")
    public Group getGroupById(@PathVariable String id) {
        return groupService.getGroupById(id);
    }

    @GetMapping("/getName/{id}")
    public String getGroupNameById(@PathVariable String id) {
        return groupService.getGroupNameById(id);
    }

/*kjbkhj*/

    @GetMapping("/get-all/")
    public List<Group> getAll() {
        return groupService.getAllGroup().collect(Collectors.toList());
    }

    @PutMapping("/update/{id}")
    public Group updateGroupById(@PathVariable String id, @RequestParam String groupName) {
        return groupService.updateGroup(id, groupName);
    }

    @DeleteMapping("delete-group-by-id/{id}")
    public String deleteById(@PathVariable String id) throws GroupNotFoundException {
        return groupService.deleteGroupById(id);
    }
    @DeleteMapping("delete-person-by-id/{id}")
    public void deletePersonById(@PathVariable String id) throws GroupNotFoundException {
        groupService.deletePersonById(id);
    }

}


