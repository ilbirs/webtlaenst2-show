package com.example.webservice2;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "GROUP_NOT_FOUND")
public class GroupNotFoundException extends Exception{
}
