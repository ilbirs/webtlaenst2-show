package com.example.webservice2;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Stream;

@Service
public class GroupService {
    Map<String, Group> groups = new HashMap<>();

    public Group createGroup(String groupName) {
        Group group1 = new Group(UUID.randomUUID().toString(), groupName, new ArrayList<>());
        groups.put(group1.getId(), group1);
        return group1;
    }

    public Group getGroupById(String id) {
        return groups.get(id);
    }

    public Stream<Group> getAllGroup() {
        return groups.values().stream();
    }

    public Group updateGroup(String id, String groupName) {
        Group group = groups.get(id);
        group.setGroupName(groupName);
        return group;
    }

    public String deleteGroupById(String id) throws GroupNotFoundException {
        boolean aBoolean = groups.containsKey(id);
        if (!aBoolean) throw new GroupNotFoundException();
        groups.remove(id);
        return id;
    }

    public Group addPersonToGroup(String personId, String groupId) {
        Group group = groups.get(groupId);
        group.getMembers().add(personId);
        return group;

    }

    public void deletePersonById(String id) {
        groups.forEach((s, group) -> group.deleteMember(id));
    }

    public String getGroupNameById(String id) {
        return groups.get(id).getGroupName();
    }
}
