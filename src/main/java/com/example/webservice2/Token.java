package com.example.webservice2;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Value;

@AllArgsConstructor
@Data
public class Token {
    String id;
    String personId;
    String groupId;
}
