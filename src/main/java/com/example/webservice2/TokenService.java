package com.example.webservice2;


import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Stream;

@Service
public class TokenService {

    Map<String, Token> tokens = new HashMap<>();

    public Token createToken() {
        Token token = new Token(UUID.randomUUID().toString(),null,null);
        tokens.put(token.getId(),token);
        return token;

    }

    public Stream<Token> getAllTokens() {
        return tokens.values().stream();
    }
}
